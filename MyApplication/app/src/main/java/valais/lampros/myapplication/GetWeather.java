package valais.lampros.myapplication;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lampros on 28-Mar-16.
 */


public class GetWeather extends AsyncTask<Void, Void, String> {
    private final String TAG = "GetWeather";
    String temperature;
    String icon;
    String summary;
    Double precipIntensity;
    Double precipProbability;
    Double realTemp;
    Double windSpeed;
    int humidity;
    private double temp;
    double lat;
    double lon;
    String summary_minutely="";
    String summary_hourly;
    List<Integer> hourly_time = new ArrayList<>();
    List<String> hourly_summary = new ArrayList<>();
    List<String> hourly_icon = new ArrayList<>();
    List<Double> hourly_temperature = new ArrayList<>();
    List<Double> hourly_wind = new ArrayList<>();


    private Exception exception;
    private WeatherFragment w;
    private ForecastFragment f;

    public GetWeather(WeatherFragment w) {
        this.w = w;
        lat = w.getLat();
        lon = w.getLon();
    }



    public void onPreExecute() {//
        Log.d(TAG, "Entered onPreExecute");
    }

    @Override
    public String doInBackground(Void... urls) {
        Log.d(TAG, "Entered in doInBackground");
        try {
            URL url = new URL("https://api.forecast.io/forecast/096bb6fb2aec629aa90a229faaff3f18/" + lat + "," + lon);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                Log.d(TAG, stringBuilder.toString());
                return stringBuilder.toString();
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            Log.d("ERROR", e.getMessage(), e);
            return null;
        }
    }

    protected void onPostExecute(String response) {
        Log.d(TAG, "Entered onPostExecute");
        if (response == null) {
            response = "THERE WAS AN ERROR :";
        }
        try {
            JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
            String timezone = object.getString("timezone");

            // currently block---------------------------------------------------
            JSONObject currently = object.getJSONObject("currently");
            temperature = currently.getString("temperature");
            icon = currently.getString("icon");
            summary = currently.getString("summary");
            precipIntensity = Double.parseDouble(currently.getString("precipIntensity"));
            precipProbability = Double.parseDouble(currently.getString("precipProbability"));
            realTemp = Double.parseDouble(currently.getString("apparentTemperature"));
            windSpeed = Double.parseDouble(currently.getString("windSpeed"));
            Double h = Double.parseDouble(currently.getString("humidity"));
            humidity = (int) (h * 100);

            // F degrees to C convertion to sent C as default
            temp = Double.parseDouble(temperature);
            temp = (temp - 32) * 0.5556;
            Log.d(TAG, "currently block finished");
//
//            // minutely block----------------------------------------------------
            // minutely doesn't exist for every location
            try {
                JSONObject minutelyObject = object.getJSONObject("minutely");
                summary_minutely=minutelyObject.getString("summary");
                Log.d(TAG,"Summary minutely is: "+summary_minutely);
            } catch (JSONException e) {
                e.printStackTrace();
            }



            // hourly block------------------------------------------------------
            JSONObject hourlyObject = object.getJSONObject("hourly");
            summary_hourly = hourlyObject.getString("summary");
            Log.d(TAG, "Summary hourly is: " + summary_hourly);
            JSONArray hourly_data = hourlyObject.getJSONArray("data");
            for (int i = 0; i < 8; i++) {
                JSONObject dataObject = hourly_data.getJSONObject(i);
                hourly_time.add(dataObject.getInt("time"));
                hourly_summary.add(dataObject.getString("summary"));
                hourly_icon.add(dataObject.getString("icon"));
                String temperature=dataObject.getString("temperature");
                Double temp =  Double.parseDouble(temperature);
                temp = (temp - 32) * 0.5556;
                //hourly_temperature.add(dataObject.getDouble("temperature"));
                hourly_temperature.add(temp);
                hourly_wind.add(dataObject.getDouble("windSpeed"));
            }


            Log.d(TAG, "finish postExecute and hourly time list length is:"+hourly_time.size());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        w.updateWeather(temp, icon, summary, windSpeed, humidity,summary_minutely);

      w.setWeatherValues(summary_hourly, hourly_time, hourly_summary, hourly_icon, hourly_temperature, hourly_wind);
    }
}


