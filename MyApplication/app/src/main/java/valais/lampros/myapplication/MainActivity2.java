package valais.lampros.myapplication;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MainActivity2 extends AppCompatActivity implements WeatherFragment.OnFragmentInteractionListener, ForecastFragment.OnFragmentInteractionListener,
        ActivitiesFragment.OnFragmentInteractionListener, Communicator  {
private WeatherFragment m1stFragment;
    private ForecastFragment m2ndFragment;
    private ActivitiesFragment m3rdFragment;
    private WeatherFragment weatherFragment;
    private SharedPreferences preferences = null;
    String TAG = "MainActivity";
    int orientation;

    DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
    String currentDateandTime = df.format(new Date());

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (haveNetworkConnection()) {
            setContentView(R.layout.activity_main2);

            preferences = PreferenceManager.getDefaultSharedPreferences(this);
            if (preferences.getBoolean("autoOrientation", true)) {
                Log.d(TAG, "orientation is user");
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
            } else {
                Log.d(TAG, "orientation is portrait");
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }

            orientation = this.getResources().getConfiguration().orientation;

            if (preferences.getBoolean(getString(R.string.pref_needs_setup), true)) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("location", "Edinburgh, UK");
                editor.commit();
                Toast.makeText(MainActivity2.this, "OOPS you must configure the app malaka", Toast.LENGTH_SHORT).show();
                startSettingsActivity();
            } else {
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                setSupportActionBar(toolbar);
                toolbar.setLogo(R.drawable.new2_launcher);

                // Create the adapter that will return a fragment for each of the three
                // primary sections of the activity.
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

                // Set up the ViewPager with the sections adapter.
                mViewPager = (ViewPager) findViewById(R.id.container);
                mViewPager.setAdapter(mSectionsPagerAdapter);

                TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
                tabLayout.setupWithViewPager(mViewPager);
                // Avoid refreshing of the fragment , 3 is the tabs limit before the refresh
                mViewPager.setOffscreenPageLimit(3);
            }

        } else {
            Toast.makeText(this, "There is not active internet connection, Please check your settings.", Toast.LENGTH_LONG).show();
            setContentView(R.layout.no_connection);
            createNetErrorDialog();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                Intent refreshScreen = new Intent(MainActivity2.this, MainActivity2.class);
                this.startActivity(refreshScreen);
                Toast.makeText(MainActivity2.this, "Refreshing", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.settings:
                Toast.makeText(MainActivity2.this, "Settings", Toast.LENGTH_SHORT).show();
                startSettingsActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFragmentInteraction() {

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
//        public static PlaceholderFragment newInstance(int sectionNumber) {
//            PlaceholderFragment fragment = new PlaceholderFragment();
//            Bundle args = new Bundle();
//            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//            fragment.setArguments(args);
//            return fragment;
//        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.activity_main2, container, false);
            // TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            // textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //   return PlaceholderFragment.newInstance(position + 1);
            switch (position) {
                case 0:

                   // Fragment WeatherFragment = new WeatherFragment();
                    //return WeatherFragment;
                    return new WeatherFragment();
                case 1:
//                    Fragment ForecastFragment = new ForecastFragment();
//                    return ForecastFragment;
                    return new ForecastFragment();
                case 2:
//                    Fragment ActivitiesFragment = new ActivitiesFragment();
//                    return ActivitiesFragment;
                    return new ActivitiesFragment();

            }
            return null;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
            // save the appropriate reference depending on position
            switch (position) {
                case 0:
                    m1stFragment = (WeatherFragment) createdFragment;
                    break;
                case 1:
                    m2ndFragment = (ForecastFragment) createdFragment;
                    break;
                case 2:
                    m3rdFragment = (ActivitiesFragment) createdFragment;
                    break;
            }
            return createdFragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Today\n"+currentDateandTime;
                case 1:
                    return "8-hour forecast";
                case 2:
                    return "Activities";
            }
            return null;
        }
    }




    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    protected void createNetErrorDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("You need a network connection to use this application. Please turn on mobile network or Wi-Fi in Settings.")
                .setTitle("Unable to connect")
                .setCancelable(false)
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                MainActivity2.this.finish();
                                Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                                startActivity(i);
                            }
                        }
                )
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                MainActivity2.this.finish();
                            }
                        }
                );
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void startSettingsActivity() {
        //   Intent intent = new Intent(this, SettingsActivity.class);
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }
public onFragmentInteraction OnFragmentInteraction;
    public interface onFragmentInteraction {
         void updateData(String data);

    }

    @Override
    public void updateData(String data) {
        // TODO Auto-generated method stub
        OnFragmentInteraction.updateData(data);

    }












    @Override
    public void respond(String summaryHourly, List<Integer> timeList, List<String> summaryList, List<String> iconList, List<Double> temperatureList, List<Double> windList){

        ForecastFragment f = m2ndFragment;
        f.changeData(summaryHourly,timeList,summaryList,iconList,temperatureList,windList);
        ActivitiesFragment a = m3rdFragment;
        a.changeData(summaryHourly,timeList,summaryList,iconList,temperatureList,windList);



    }






}
