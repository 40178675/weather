package valais.lampros.myapplication;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class PhotoTask extends AsyncTask<Void, Void, String> {
    String TAG = "PhotoTask";
    String title = "";
    int max;
    String orientation;
    int width;
    int height;


    String searchQ = "";
    private WeatherFragment w;

    public PhotoTask(WeatherFragment w) {
        this.w = w;
        searchQ = w.city;
        if (w.orientation == 1) {
            orientation = "portrait";
        } else if (w.orientation == 2) {
            orientation = "landscape";
        }


        // searchQ=searchQ.replace(',','+');
        searchQ = searchQ.replace(" ", "");
        String[] line = searchQ.split(",");
        searchQ = line[0];
        Log.d(TAG, "before flickrAPi city is :" + searchQ);
    }

    public void onPreExecute() {
        Log.d(TAG, "Entered onPreExecute");
    }


    // Apply your Flickr API:
// www.flickr.com/services/apps/create/apply/?
    String FlickrApiKey = "77897930fcd1a629b61ceb9ee11d6deb";

    private static final String FLICKRAPIKEY = "77897930fcd1a629b61ceb9ee11d6deb";

    @Override
    public String doInBackground(Void... urls) {
        //private String flickrApi(String searchPattern, int limit) throws IOException, JSONException {
        StringBuilder stringBuilder;
        try {
            //searchQ="";
            //URL url = new URL("http://api.flickr.com/services/rest/?method=flickr.photos.search&text=" + searchPattern + "&api_key=" + FLICKRAPIKEY + "&per_page="+ limit + "&format=json");
            //  URL url2 = new URL("https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key="+FLICKRAPIKEY+"&tags=Edinburgh&safe_search=1&per_page=1&format=json");
            URL url = new URL("https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=77897930fcd1a629b61ceb9ee11d6deb&text=" + searchQ + "&per_page=50&format=json&sort=relevance&safe_search=1&content_type=1&accuracy=9&media=photos&orientation=" + orientation + "&tag_mode=all&dimension_search_mode=min&height=" + height + "&width=" + width);

            //URL url = new URL("https://api.flickr.com/services/rest/?method=flickr.stats.getPopularPhotos&api_key=77897930fcd1a629b61ceb9ee11d6deb&tags=" + searchQ + "&per_page=10&format=json");
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                Log.d(TAG, stringBuilder.toString());
                return stringBuilder.toString();
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            Log.d("ERROR in PhotoTask", e.getMessage(), e);
            return null;
        }
    }

    protected void onPostExecute(String response) {
        JSONObject photo = new JSONObject();
        try {
            int start = response.toString().indexOf("(") + 1;
            int end = response.toString().length() - 1;
            String jSONString = response.toString().substring(start, end);
            //after cutting off the junk, its ok

            JSONObject jSONObject = new JSONObject(jSONString); //whole json object
            JSONObject jSONObjectInner = jSONObject.getJSONObject("photos"); //inner Json object
            JSONArray photoArray = jSONObjectInner.getJSONArray("photo"); // inner array of photos
            max = 0;
            Log.d(TAG, "photoArray size is :" + photoArray.length());
            if (photoArray.length() > 70) {
                max = 40;
            } else if (photoArray.length() > 40) {
                max = 30;
            } else if (photoArray.length() > 20) {
                max = 20;
            } else if (photoArray.length() > 15) {
                max = 10;
            } else if (photoArray.length() > 10) {
                max = 5;
            } else if (photoArray.length() > 0) {
                max = photoArray.length() - 1;
            } else {
                max = 0;
            }

            Log.d(TAG, "MAX size is :" + max);
            Log.d(TAG, "Orientation is: " + orientation);
            int min = 0;
            if (max != 0) {
                int number = (int) Math.floor(Math.random() * (max - min + 1)) + min;
                Log.d(TAG, "Joker is :" + number);
                photo = photoArray.getJSONObject(number); //get one random photo from array
                title = photo.getString("title");
                Log.d(TAG, "finished PostExecute");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (max > 0) {
            if (photo != null) {
                try {
                    Log.d(TAG, constructFlickrImgUrl(photo, size._c));
                    new LoadBackground(w, constructFlickrImgUrl(photo, size._c), "androidfigure").execute();
                    w.signPhoto(title);
                    Log.d(TAG, title);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            Log.d(TAG, "No image found, calling the default bg");
            w.loadDefaultBackground();
        }


    }

    enum size {
        _s, _t, _m, _c, _b, _o
        // 800x600   -->_c
        // 1024x768  -->_b
        // original  -->_o
    }

    ;

    //helper method, to construct the url from the json object. You can define the size of the image that you want, with the size parameter.
    //  Be aware that not all images on flickr are available in all sizes.
    private String constructFlickrImgUrl(JSONObject input, Enum size) throws JSONException {

        String FARMID = input.getString("farm");
        String SERVERID = input.getString("server");
        String SECRET = input.getString("secret");
        String ID = input.getString("id");

        StringBuilder sb = new StringBuilder();

        sb.append("http://farm");
        sb.append(FARMID);
        sb.append(".static.flickr.com/");
        sb.append(SERVERID);
        sb.append("/");
        sb.append(ID);
        sb.append("_");
        sb.append(SECRET);
        sb.append(size.toString());
        sb.append(".jpg");

        return sb.toString();
    }
}


