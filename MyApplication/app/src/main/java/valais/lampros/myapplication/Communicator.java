package valais.lampros.myapplication;

import java.util.List;

/**
 * Created by Lampros on 03-Apr-16.
 */
public interface Communicator {
    public void respond(String summaryHourly, List<Integer> timeList, List<String> summaryList, List<String> iconList, List<Double> temperatureList, List<Double> windList);
}
