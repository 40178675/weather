/**
 * The MIT License (MIT)
 * <p>
 * Copyright (c) 2015 Yoel Nunez <dev@nunez.guru>
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package valais.lampros.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;


public class SettingsActivity extends AppCompatActivity {
    private AppCompatDelegate delegate;
    String TAG = "SettingsActivity";
    private SharedPreferences preferences = null;
    Switch mySwitch;
    Switch photoSwitch;
    Switch orientationSwitch;
    String location;
    co.geeksters.googleplaceautocomplete.lib.CustomAutoCompleteTextView autoText;
    private RadioGroup radioTemp;
    private RadioButton radioButton;
    private RadioButton radioButton1;
    private RadioButton radioButton2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        getDelegate().installViewFactory();
        getDelegate().onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        checkContent();
        mySwitch = (Switch) findViewById(R.id.locationSwitch);
        photoSwitch = (Switch) findViewById(R.id.photoSwitch);
        orientationSwitch = (Switch) findViewById(R.id.orientationSwitch);

        radioTemp = (RadioGroup) findViewById(R.id.radioTemp);
        radioButton1 = (RadioButton) findViewById(R.id.radioButton);
        radioButton2 = (RadioButton) findViewById(R.id.radioButton2);

        if (preferences.getBoolean("autoOrientation", true)) {
            Log.d(TAG, "orientation is user");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
        } else {
            Log.d(TAG, "orientation is portrait");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        if (preferences.getBoolean("geoloc", true)) {
            mySwitch.setChecked(true);
            Log.d("SKATA", "skata");
            autoText.setEnabled(false);
        } else {
            mySwitch.setChecked(false);
            autoText.setEnabled(true);
        }
        if (preferences.getBoolean("background", true)) {
            photoSwitch.setChecked(true);

        } else {
            photoSwitch.setChecked(false);
        }

        if (preferences.getBoolean("tempCelsius", true)) {
            radioButton1.setChecked(true);
        } else {
            radioButton2.setChecked(true);
        }
        if (preferences.getBoolean("autoOrientation", true)) {
            orientationSwitch.setChecked(true);
        } else {
            orientationSwitch.setChecked(false);
        }


        if (!preferences.getBoolean(getString(R.string.pref_needs_setup), false)) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(getString(R.string.pref_needs_setup), false);
            editor.apply();
        }


        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                SharedPreferences.Editor editor = preferences.edit();
                if (isChecked) {
                    autoText.setEnabled(false);
                    editor.putBoolean("geoloc", true);
                } else {
                    autoText.setEnabled(true);
                    editor.putBoolean("geoloc", false);
                }
                editor.commit();
            }
        });

        photoSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                SharedPreferences.Editor editor = preferences.edit();
                if (isChecked) {
                    editor.putBoolean("background", true);
                } else {
                    editor.putBoolean("background", false);
                }
                editor.commit();
            }
        });

        orientationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                SharedPreferences.Editor editor = preferences.edit();
                if (isChecked) {
                    editor.putBoolean("autoOrientation", true);
                } else {
                    editor.putBoolean("autoOrientation", false);
                }
                editor.commit();
            }
        });

    }

    private void startMainActivity() {
        //   Intent intent = new Intent(this, SettingsActivity.class);
        Intent intent = new Intent(this, MainActivity2.class);
        startActivity(intent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.done_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.done:

                if (!isEmpty(autoText)) {
                    Log.d(TAG, "Autotext is not empty");

                    location = autoText.googlePlace.getDescription();
                    Log.d("doneButton", "Fucking done button clicked and fucking location is:" + location);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("location", location);
                    editor.commit();
                } else {
                    Log.d(TAG, "Autotext is empty");

                    Log.d("doneButton", "Fucking done button clicked and fucking location is:" + location);
                    location = autoText.getHint().toString();
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("location", location);
                    editor.commit();
                }
                // int selectedId = radioTemp.getCheckedRadioButtonId();
                //radioButton =(RadioButton)findViewById(selectedId);
                SharedPreferences.Editor editor = preferences.edit();
                if (radioButton1.isChecked()) {
                    //Toast.makeText(SettingsActivity.this, radioButton1.getText(), Toast.LENGTH_LONG).show();
                    editor.putBoolean("tempCelsius", true);


                } else {
                    //  Toast.makeText(SettingsActivity.this, radioButton2.getText(), Toast.LENGTH_LONG).show();
                    editor.putBoolean("tempCelsius", false);
                }
                editor.commit();
                startMainActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean isEmpty(co.geeksters.googleplaceautocomplete.lib.CustomAutoCompleteTextView etText) {
        return etText.getText().toString().trim().length() == 0;
    }

    private void checkContent() {
        Log.d(TAG, "entered checkContent");
        autoText = (co.geeksters.googleplaceautocomplete.lib.CustomAutoCompleteTextView) findViewById(R.id.atv_places);
        autoText.setHint(preferences.getString("location", ""));

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getDelegate().onPostCreate(savedInstanceState);
    }

    public ActionBar getSupportActionBar() {
        return getDelegate().getSupportActionBar();
    }

    public void setSupportActionBar(@Nullable Toolbar toolbar) {
        getDelegate().setSupportActionBar(toolbar);
    }

    @Override
    public MenuInflater getMenuInflater() {
        return getDelegate().getMenuInflater();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        getDelegate().setContentView(layoutResID);
    }

    @Override
    public void setContentView(View view) {
        getDelegate().setContentView(view);
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        getDelegate().setContentView(view, params);
    }

    @Override
    public void addContentView(View view, ViewGroup.LayoutParams params) {
        getDelegate().addContentView(view, params);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        getDelegate().onPostResume();
    }

    @Override
    protected void onTitleChanged(CharSequence title, int color) {
        super.onTitleChanged(title, color);
        getDelegate().setTitle(title);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getDelegate().onConfigurationChanged(newConfig);
    }

    @Override
    protected void onStop() {
        super.onStop();
        getDelegate().onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getDelegate().onDestroy();
    }

    public void invalidateOptionsMenu() {
        getDelegate().invalidateOptionsMenu();
    }

    public AppCompatDelegate getDelegate() {
        if (delegate == null) {
            delegate = AppCompatDelegate.create(this, null);
            setTitle("Settings");
        }
        return delegate;
    }
}
