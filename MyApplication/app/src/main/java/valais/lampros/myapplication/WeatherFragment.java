package valais.lampros.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WeatherFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WeatherFragmentnewInstance} factory method to
 * create an instance of this fragment.
 */
public class WeatherFragment extends Fragment {

    Communicator com;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";



    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String summary_hourly="";
    List<Integer> hourly_time = new ArrayList<>();
    List<String> hourly_summary = new ArrayList<>();
    List<String> hourly_icon = new ArrayList<>();
    List<Double> hourly_temperature = new ArrayList<>();
    List<Double> hourly_wind = new ArrayList<>();


    RelativeLayout relativeLayout;
    Context thiscontext;
    private static String TAG = "WeatherFragment";
    private double lat;
    private double lon;
    String city;
    String location = null;
    private ProgressDialog dialog;
    private SharedPreferences preferences = null;
    TextView degreesView;
    TextView summaryView;
    TextView laterSummary;
    TextView coordinatesView;
    TextView windView;
    TextView caption;
    TextView provider;
    ImageView gif;
    GpsTracker gps;
    PhotoTask photoTask;
    RelativeLayout circle;
    int orientation;
    int width;
    int height;
    String IconFinal;
    String test;
    TextView humid;


    private OnFragmentInteractionListener mListener;

    public WeatherFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * <p/>
     * //   * @param param1 Parameter 1.
     * //   * @param param2 Parameter 2.
     * //  * @return A new instance of fragment WeatherFragment.
     */
//    // TODO: Rename and change types and number of parameters
//    public static WeatherFragment newInstance(String param1, String param2) {
//        WeatherFragment fragment = new WeatherFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }

    // TODO: Rename and change types and number of parameters

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

//
//        relativeLayout = (RelativeLayout)onCreateView.findViewById(R.id.background);
//        circle = (RelativeLayout)getView().findViewById(R.id.circle);
//        Log.d(TAG, "Entered onCreate");
//        gif = (ImageView)getView(). findViewById(R.id.gifView);
//        degreesView = (TextView)getView(). findViewById(R.id.degrees);
//        summaryView = (TextView)getView(). findViewById(R.id.summary);
//        coordinatesView = (TextView)getView(). findViewById(R.id.coordinates);
//        windView = (TextView)getView(). findViewById(R.id.wind);
//        caption = (TextView)getView(). findViewById(R.id.caption);
//        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
//        //1 for Portrait and 2 for Landscape
//        orientation = this.getResources().getConfiguration().orientation;
//        Log.d(TAG, "width is:" + getScreenWidth() + " and height is: " + getScreenHeight());
//        width = getScreenWidth();
//        height = getScreenHeight();
//
//        showDiag();
//        if (preferences.getBoolean("geoloc", true)) {
//            Log.d(TAG, "GEO is enabled");
//            getLocation();
//            getWeather();
//            //
//        } else {
//            test = preferences.getString("location", "");
//            Log.d(TAG, "GEO is disabled, LOCATION IS :" + test);
//            location = test;
//            getCoordinates();
//            //updateWeather(test, "");
//        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  return inflater.inflate(R.layout.fragment_main, container, false);
        View v = inflater.inflate(R.layout.fragment_weather, container, false);
        relativeLayout = (RelativeLayout) v.findViewById(R.id.background);
        circle = (RelativeLayout) v.findViewById(R.id.circle);
        Log.d(TAG, "Entered onCreateView");
        gif = (ImageView) v.findViewById(R.id.gifView);
        degreesView = (TextView) v.findViewById(R.id.degrees);
        summaryView = (TextView) v.findViewById(R.id.summary);
        coordinatesView = (TextView) v.findViewById(R.id.coordinates);
        windView = (TextView) v.findViewById(R.id.wind);
        caption = (TextView) v.findViewById(R.id.caption);
        provider = (TextView) v.findViewById(R.id.powered_by);
        humid = (TextView) v.findViewById(R.id.humid);
        laterSummary =(TextView)v.findViewById(R.id.laterSummary);

        thiscontext = container.getContext();
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        //1 for Portrait and 2 for Landscape
        orientation = this.getResources().getConfiguration().orientation;
        Log.d(TAG, "width is:" + getScreenWidth() + " and height is: " + getScreenHeight());
        width = getScreenWidth();
        height = getScreenHeight();
        relativeLayout.setVisibility(View.GONE);
        showDiag();
        if (preferences.getBoolean("geoloc", true)) {
            Log.d(TAG, "GEO is enabled");
            getLocation();
            getWeather();
            //
        } else {
            test = preferences.getString("location", "");
            Log.d(TAG, "GEO is disabled, LOCATION IS :" + test);
            location = test;
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("final_location", location);
            editor.commit();
            getCoordinates();
            //updateWeather(test, "");
        }

        return v;
    }

//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction();
//        }
//    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
com=(Communicator)context;




    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public OnFragmentInteractionListener onFragmentInteractionListener;

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name

        void updateData(String location2);


    }


    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }


    public void getCoordinates() {
        GetCoordinates coordinates = new GetCoordinates(this);
        coordinates.execute();
    }

    public void updateCoordinates(Double latitude, Double longitude) {
        lat = latitude;
        lon = longitude;
        Log.d(TAG, "Coordinates updated! Lat:" + lat + ", Lon:" + lon);
        getWeather();
    }

    public String getCity() {
        return location;
    }

    public void showDiag() {

        Log.d(TAG, "showDiag called");

        dialog = new ProgressDialog(getContext());
        dialog.setMessage(getString(R.string.loading));
        dialog.setCancelable(false);
        dialog.show();
    }

    public void getLocation() {
        gps = new GpsTracker(this.getContext());
        lat = gps.getLatitude();
        lon = gps.getLongitude();
        // Toast.makeText(this, "Latitude: " + lat + "Longitude: " + lon, Toast.LENGTH_LONG).show();
        Log.d(TAG, " " + lat + " -- " + lon);
    }

    public void getWeather() {
        GetWeather g = new GetWeather(this);
        g.execute();
    }

    public void getPlace() {
        GetPlace p = new GetPlace(this);
        p.execute();
    }


    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }


    public void updateWeather(Double temp, String icon, String summary, Double wind, int humidity,String later_Summary) {
        // preferences = PreferenceManager.getDefaultSharedPreferences(this);
        // if(preferences.getString(getString(R.string.pref_temperature_unit), null)=="C")
        if (preferences.getBoolean("tempCelsius", true)) {
            degreesView.setText(String.format("%.1f", temp) + "\u00B0" + " C"); // Round to 1 decimal
        } else {
            degreesView.setText(String.format("%.1f", (temp * 1.8) + 32) + "\u00B0" + " F"); // Round to 1 decimal
        }
        degreesView.setTextColor(getResources().getColor(R.color.white));
        Log.d(TAG, "Icon is:" + icon);
        humid.setText(humidity + "%");
        humid.setTextColor(getResources().getColor(R.color.white));
        laterSummary.setText(later_Summary);
        laterSummary.setTextColor(getResources().getColor(R.color.white));


        IconFinal = icon.replace('-', '_'); // Can't have icon names with  '-'
        // weatherIconImageView.setImageResource(R.drawable.icon_30);
        String PACKAGE_NAME = getContext().getPackageName();
        int imgId = getResources().getIdentifier(PACKAGE_NAME + ":drawable/" + IconFinal, null, null);
        //weatherView.setImageBitmap(BitmapFactory.decodeResource(getResources(), imgId));
        gif.setImageBitmap(BitmapFactory.decodeResource(getResources(), imgId));
        summaryView.setText(summary);
        summaryView.setTextColor(getResources().getColor(R.color.white));
        windView.setText((int) Math.round(wind) + " Mph");
        windView.setTextColor(getResources().getColor(R.color.white));
        Log.d(TAG, "Weather details are set");
        if (preferences.getBoolean("geoloc", true)) {
            Log.d(TAG, "GEoloc enable call getPlace for location");
            getPlace();
        } else {
            Log.d(TAG, "geoloc is off so call updateWeather from test");
            updateWeather(test, "");
        }

    }

    // Set city and postcode and start the relevant photo search
    public void updateWeather(String c, String postcode) {
        Log.d(TAG, "Entered updateWeater( , )");
        coordinatesView.setText(c + " " + postcode);
        coordinatesView.setTextColor(getResources().getColor(R.color.white));
        provider.setTextColor(getResources().getColor(R.color.white));
        provider.getBackground().setAlpha(150);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("final_location", c + " " + postcode);

        editor.commit();
        Log.d(TAG, "Final location is: " + preferences.getString("final_location", null));
        city = c;
        // Load the background
        // Set the background photo
        if (preferences.getBoolean("background", true)) {
            Log.d(TAG, "Need to get photo online");
            new PhotoTask(this).execute();
        } else {
            Log.d(TAG, "Need to set default bg");
            loadDefaultBackground();
        }
    }

    public void hideLoad() {
        relativeLayout.setVisibility(View.VISIBLE);
        dialog.hide();
        //  mListener.updateData("SkATA");
       // com.respond("SKATA STON TAFO TOU ANDROID");

    }

    public void loadDefaultBackground() {
        String PACKAGE_NAME = getContext().getPackageName();
        int imgId = getResources().getIdentifier(PACKAGE_NAME + ":drawable/" + IconFinal + "_bg" + orientation, null, null);
        //weatherView.setImageBitmap(BitmapFactory.decodeResource(getResources(), imgId));
        Log.d(TAG, "IconFinal is: " + IconFinal + "_bg" + orientation);

        // TODO if that photo exists
        if (orientation == 1) {
            //Portrait mode
            relativeLayout.setBackground(ContextCompat.getDrawable(getContext(), imgId));

        } else {
            //Landscape mode
            relativeLayout.setBackground(ContextCompat.getDrawable(getContext(), imgId));
        }
        circle.getBackground().setAlpha(150);
        caption.setVisibility(View.INVISIBLE);
        hideLoad();
    }

    public void signPhoto(String title) {
        //  Toast.makeText(this, title, Toast.LENGTH_LONG).show();
        // Set background photo transparency (min 0 transparent - max 255 completely opaque)
        circle.getBackground().setAlpha(150);
        caption.getBackground().setAlpha(150);
        // Cut title if it's too big and is going to overlap other views.
        if (title.length() > 28) {
            title = title.substring(0, 28) + "... ";
        }
        caption.setText(title + " by Flickr");
        caption.setTextColor(getResources().getColor(R.color.white));

    }


    public void setWeatherValues(String summaryHourly, List<Integer> timeList, List<String> summaryList, List<String> iconList, List<Double> temperatureList, List<Double> windList){
       Log.d(TAG,"preparing to send data to forecastFragment");
        com.respond(summaryHourly,timeList,summaryList,iconList,temperatureList,windList);
    }


}
