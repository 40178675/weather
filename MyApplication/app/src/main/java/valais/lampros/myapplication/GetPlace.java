package valais.lampros.myapplication;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Lampros on 29-Mar-16.
 */


class GetPlace extends AsyncTask<Void, Void, String> {

    private final String TAG = "GetPlace";
    private String APIKEY = "AIzaSyDXomyO9dTFQEuIHzzooJRiJI69xD4VhzE";
    private WeatherFragment w;
    private Exception exception;
    private double lat;
    private double lon;
    private String long_name1 = "";
    private String long_name2 = "";
    private String long_name = "";


    public GetPlace(WeatherFragment w) {
        this.w = w;
        lat = w.getLat();
        lon = w.getLon();
    }

    protected void onPreExecute() {
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
        Log.d(TAG, "Entered onPreExecute");
    }

    protected String doInBackground(Void... urls) {

        Log.d(TAG, "Entered in doInBackground");
        // Do some validation here
        //  final String cityText = city + ",+" + country;

        try {
            URL url = new URL(" https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&key=" + APIKEY);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                Log.d("line", stringBuilder.toString());
                return stringBuilder.toString();
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            Log.d("ERROR", e.getMessage(), e);
            return null;
        }
    }

    protected void onPostExecute(String response) {
        Log.d(TAG, "Entered onPostExecute");
        if (response == null) {
            response = "THERE WAS AN ERROR :";
        }
        // progressBar.setVisibility(View.GONE);
        // Log.d("test", response);
        // responseView.setText(response);
        // TODO: check this.exception
        // TODO: do something with the feed

        try {
            Log.d(TAG, "inside TRY function");
            JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
            JSONArray array = object.getJSONArray("results");
            //JSONObject obj = array.getJSONObject("address_components");

            for (int i = 0; i < array.length(); i++) {

                JSONObject c = array.getJSONObject(i);
                JSONArray arrayB = c.getJSONArray("address_components");
                for (int j = 0; j < arrayB.length(); j++) {
                    JSONObject skata = arrayB.getJSONObject(j);
                    // City
                    if (j == 3) {
                        long_name1 = skata.getString("long_name");
                    }
                    // postcode
                    else if (j == 6) {
                        long_name2 = skata.getString("long_name");
                    }


                }
                long_name = long_name1 + long_name2;
                break;

                //  long_name = c.getString("long_name");
            }


            Log.d(TAG, long_name);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        w.updateWeather(long_name1, long_name2);
        new ForecastFragment().setValues(long_name1, long_name2);

    }


}
