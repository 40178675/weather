package valais.lampros.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ActivitiesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ActivitiesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ActivitiesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    TextView loc;
    TextView time1;
    RelativeLayout relativeLayout;
    Context thiscontext;
    private Context mContext;

    private double lat;
    private double lon;
    String city;
    String location = null;
    private ProgressDialog dialog;
    private SharedPreferences preferences = null;
    TextView degreesView;
    TextView summaryView;
    TextView coordinatesView;
    TextView windView;
    TextView caption;
    TextView provider;
    ImageView gif;
    GpsTracker gps;
    PhotoTask photoTask;
    RelativeLayout circle;
    int orientation;
    int width;
    int height;
    String IconFinal;
    String test;
    TextView humid;
    View v;
    LinearLayout linearLayout;

    // WeatherData
    String summary_hourly = "";
    List<Integer> hourly_time = new ArrayList<>();
    List<String> hourly_summary = new ArrayList<>();
    List<String> hourly_icon = new ArrayList<>();
    List<Double> hourly_temperature = new ArrayList<>();
    List<Double> hourly_wind = new ArrayList<>();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String TAG = "ActivitiesFragment";

    private OnFragmentInteractionListener mListener;

    public ActivitiesFragment() {
        // Required empty public constructor
    }

    public void changeData(String summaryHourly, List<Integer> timeList, List<String> summaryList, List<String> iconList, List<Double> temperatureList, List<Double> windList) {
        Log.d(TAG, "FINALLY SOME DATA");
        summary_hourly = summaryHourly;
        hourly_time = timeList;
        hourly_summary = summaryList;
        hourly_icon = iconList;
        hourly_temperature = temperatureList;
        hourly_wind = windList;
        Log.d(TAG, "Finally lists are saved!!");
         displayData();
    }

    // TODO: Rename and change types and number of parameters
    public static ActivitiesFragment newInstance(String param1, String param2) {
        ActivitiesFragment fragment = new ActivitiesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_activities, container, false);
        thiscontext = container.getContext();
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        linearLayout = (LinearLayout) v.findViewById(R.id.root);


        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction();
    }

//Add layouts with icons
    public void displayData() {

        int myId = 1;
        LayoutInflater inflater = (LayoutInflater) thiscontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < 4; i++) {
            View myLayout = inflater.inflate(R.layout.activities_item, null);
            LinearLayout.LayoutParams params;
            linearLayout.addView(myLayout);
            params = (LinearLayout.LayoutParams) myLayout.getLayoutParams();
            params.height = 100;
            params.topMargin = 10;
            myLayout.setLayoutParams(params);
            ImageView icon = (ImageView)myLayout.findViewById(R.id.imageView3);
            icon.setImageResource(R.drawable.weather);


        }
    }
}
