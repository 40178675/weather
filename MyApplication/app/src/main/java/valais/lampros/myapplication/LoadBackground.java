package valais.lampros.myapplication;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Lampros on 29-Mar-16.
 */
class LoadBackground extends AsyncTask<String, Void, Drawable> {

    private String imageUrl, imageName;
    private WeatherFragment w;

    public LoadBackground(WeatherFragment w, String url, String file_name) {
        this.w = w;
        this.imageUrl = url;
        this.imageName = file_name;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Drawable doInBackground(String... urls) {

        try {
            InputStream is = (InputStream) this.fetch(this.imageUrl);
            Drawable d = Drawable.createFromStream(is, this.imageName);
            return d;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Object fetch(String address) throws MalformedURLException, IOException {
        URL url = new URL(address);
        Object content = url.getContent();
        return content;
    }

    @Override
    protected void onPostExecute(Drawable result) {
        super.onPostExecute(result);
        // w.changeBackground(result);
        w.relativeLayout.setBackgroundDrawable(result);
        w.hideLoad();
        //relativeLayout.setBackgroundDrawable(result);
    }
}
