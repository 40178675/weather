package valais.lampros.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ForecastFragment extends Fragment implements WeatherFragment.OnFragmentInteractionListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    TextView loc;
    Context thiscontext;
    String city;
    String location = null;
    private SharedPreferences preferences = null;
    String IconFinal;
    View v;
    String TAG = "ForecastFragment";
    // WeatherData
    String summary_hourly = "";
    List<Integer> hourly_time = new ArrayList<>();
    List<String> hourly_summary = new ArrayList<>();
    List<String> hourly_icon = new ArrayList<>();
    List<Double> hourly_temperature = new ArrayList<>();
    List<Double> hourly_wind = new ArrayList<>();
    View view;
    LinearLayout linearLayout;



   // This will receive the data from WeatherFragment
    public void changeData(String summaryHourly, List<Integer> timeList, List<String> summaryList, List<String> iconList, List<Double> temperatureList, List<Double> windList) {
        Log.d(TAG, "FINALLY SOME DATA");
        summary_hourly = summaryHourly;
        hourly_time = timeList;
        hourly_summary = summaryList;
        hourly_icon = iconList;
        hourly_temperature = temperatureList;
        hourly_wind = windList;
        Log.d(TAG, "Finally lists are saved!!");
        displayData();
    }
    //String location;
    @Override
    public void updateData(String location1) {
        Log.d(TAG, "inside updateData ");
        this.location = location1;
    }


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ForecastFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ForecastFragment newInstance(String param1, String param2) {
        ForecastFragment fragment = new ForecastFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_forecast, null);
        thiscontext = container.getContext();
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        location = preferences.getString("final_location", null);
        Log.d("ForecastFragment", "Location is: " + location);
        loc = (TextView) v.findViewById(R.id.location);
        Log.d(TAG, "setting structureSet to true");
        loc = (TextView) v.findViewById(R.id.location);
        linearLayout = (LinearLayout) v.findViewById(R.id.root);
        view = v;
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "Inside onResume");
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction();
    }

    public void setValues(String location1, String location2) {
        location = location1 + ", " + location2;
        Log.d("ForecastFragment", "Inside setValues");
        Log.d(TAG, "Inside setValues Location is set to:" + location);
    }

    //This will be called when the user views that tab
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.d(TAG, "Inside setUserVisibleHint");
            loc.setText(preferences.getString("final_location", null));
        }
    }


    public void displayData() {
        loc.setText(summary_hourly);
        int myId = 1;
        LayoutInflater inflater = (LayoutInflater) thiscontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < 8; i++) {
            View myLayout = inflater.inflate(R.layout.forecast_item, null);
            //Time
            TextView timeView = (TextView) myLayout.findViewById(R.id.time1);
            long dv = Long.valueOf(hourly_time.get(i).toString()) * 1000;// its need to be in milisecond
            Date df = new java.util.Date(dv);
            String s = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm").format(df);
            String t = hourly_time.get(i).toString();
            Log.d(TAG, "Time is: " + s + " and unix is :" + t);
            timeView.setText(s);
            // Temp
            TextView tempView = (TextView) myLayout.findViewById(R.id.degrees1);
            if (preferences.getBoolean("tempCelsius", true)) {
                tempView.setText(String.format("%.1f", hourly_temperature.get(i)) + "\u00B0" + " C"); // Round to 1 decimal
                Log.d(TAG, "temp C is " + hourly_temperature.get(i));
            } else {
                tempView.setText(String.format("%.1f", (hourly_temperature.get(i) * 1.8) + 32) + "\u00B0" + " F"); // Round to 1 decimal
                Log.d(TAG, "temp F is " + hourly_temperature.get(i));
            }
            // Icon
            ImageView weatherIcon = (ImageView) myLayout.findViewById(R.id.weatherIcon);
            IconFinal = hourly_icon.get(i).replace('-', '_'); // Can't have icon names with  '-'
            Log.d(TAG, "Icon final is: " + IconFinal);
            String PACKAGE_NAME = getContext().getPackageName();
            Log.d(TAG, "Package name is: " + PACKAGE_NAME);
            int imgId = getResources().getIdentifier(PACKAGE_NAME + ":drawable/" + IconFinal, null, null);
            weatherIcon.setImageBitmap(BitmapFactory.decodeResource(getResources(), imgId));
            //wind
            TextView windView = (TextView) myLayout.findViewById(R.id.wind1);
            windView.setText((int) Math.round(hourly_wind.get(i)) + " Mph");
            //
            LinearLayout.LayoutParams params;
            linearLayout.addView(myLayout);
            params = (LinearLayout.LayoutParams) myLayout.getLayoutParams();
            params.height = 100;
            params.topMargin = 10;
            myLayout.setLayoutParams(params);

        }
    }
}
